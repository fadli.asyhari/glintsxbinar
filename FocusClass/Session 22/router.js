const router = require ('express').Router();

//Controller
const index = require('./controllers/indexController.js');
const post = require('./controllers/postController.js');

router.get('/', index.index);

router.get('/api/v1/posts', post.index);
router.post('/api/v1/posts', post.create);

module.exports = router;