const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = async (req, res, next) => {
  const token = req.headers.authorization
  
  if (!token) return res.status(401).send({ ok: false })
  
  let isVerified;
  
  try {
    isVerified = jwt.verify(token, process.env.SECRET_KEY)
  } catch (error) {
    return res.status(401).send({ ok: false })
  }

  const authenticatedUser = await User.findOne({ where: { email: isVerified.email } })

  req.user = authenticatedUser
  
  return next()
}
