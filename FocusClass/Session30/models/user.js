'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    encrypted_password: DataTypes.STRING
  }, {
    underscored: true,
  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};

User._encrpyt = password => bcrypt.hashSync(password, 10)

User.register = async function ({ email, password}) {
  return this.create({ email, encrypted_password: this._encrpyt(password)})
}