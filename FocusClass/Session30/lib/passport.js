const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

const { User } = require('../models')

async function authenticate(email, password, done) {
    try {
        await User.authenticate({ email, password })
        return done(null, user)
    }

    catch (err) {
        return done(null, false, { message: err.message })
    }
}

passport.use(
    new LocalStrategy({
        usernameField: 'email'
    }, authenticate)
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)
passport.deserializeUser(
    async (id, done) => done(null, await User.findByPk(id))
)