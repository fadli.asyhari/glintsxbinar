const { User } = require('../../models')

module.exports = {
    async login(req, res) {
        try {
            await User.authenticate(req.body)
            res.redirect('/')
        }
        catch (err) {
            res.redirect('/404')
        }
    },

    logout(req) {
        return req.logOut()
    }
}
