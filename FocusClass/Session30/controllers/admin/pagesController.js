

module.exports = {

    auth: {
        login: (req, res) => {
            if (req.isAuthenticated())
                return res.redirect('/me')
            res.status(200).render('pages/auth/login')
        },
        me: (req, res) =>
            res.status(200).render('pages/auth/me', { user: req.user })
    }
}