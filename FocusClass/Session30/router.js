const router = require('express').Router();

// Middlewares
const passport = require('./lib/passport')

// Controllers
const admin = require('./controllers/admin')
const user = require('./controllers/userController');
const post = require('./controllers/postController')

router.post('./api/v1/user/register', user.create)
router.get('/login', admin.pages.auth.login)
router.post(
    '/admin/login',
    passport.authenticate('local', {
        successRedirect: '/me',
        failureRedirect: '/login',
        failureFlash: true
    })
)
router.get('/logout', admin.auth.logout)

const authenticate = require('./middlewares/authenticate');

router.post('/user/register', user.register);
router.post('/user/login', user.login);

router.post('/posts', authenticate, post.create)
router.get('/posts', authenticate, post.all)