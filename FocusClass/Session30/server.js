const express = require('express');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const session = require('express-session')
const flash = require('express-flash')
const passport = require('./lib/passport')

app.use(express.json());
app.use(express.urlencoded({ extended: false }))
if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'))
app.use(cors());

app.use(flash())
app.use(
    session({
        secret: process.env.SECRET_KEY,
        resave: false,
        saveUninitialized: false
    })
)
app.use(passport.initialize())
app.use(passport.session())
app.set('view engine', 'pug')

require('dotenv').config();

const router = require('./router')

app.get('/', (req, res) => {
    res.status(200).json({
        "status": "success",
        "message": "Good Day, This is your to-do app"
    })
})


module.exports = app;