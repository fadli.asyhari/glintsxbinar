const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { User } = require('../models');

router.post('/register', async (req, res) => {
    // Receive request body
    const { name, email, password } = req.body

    // Check when value not provided
    if (!name || !email || !password) {
        return res.status(422).send({ ok: false })
    }

    // Encrypt the password
    const encrypted_password = bcrypt.hashSync(password);

    // Create user
    const user = await User.create({
        name,
        email,
        encrypted_password
    })

    // Generate token
    const token = jwt.sign({
        name: user.name,
        email: user.email,
    }, process.env.SECRET_KEY);

    // Send token
    res.json({ token })
})

router.post('/login', async (req, res) => {
    const { email, password } = req.body

    if (!email || !password) {
        return res.status(422).send({ ok: false})
    }

    // cari user yg punya email X
    const user = await User.findOne({
        where: {
            email
        }
    })

    // cek user ada ato ngga
    if (!user) return res.status(401).send({ ok: false })
    
    // validasi password nya
    const isValidated = bcrypt.compareSync(password, user.encrypted_password)
    if (!isValidated) return res.status(401).send({ ok: false })

    // Generate token
    const token = jwt.sign({
        name: user.name,
        email: user.email
    }, process.env.SECRET_KEY);

    // kasih token balik
    res.send({token})
})

module.exports = router;