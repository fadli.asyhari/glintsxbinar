const router = require ('express').Router();

//Controller
const index = require('./controllers/indexController.js');
const post = require('./controllers/postController.js');
const test = require('./controllers/testController');

//Import middleware
const uploader = require('./middlewares/uploader.js');
const authenticate = require('./middlewares/authenticate');

// Routes
const userRoutes = require('./routes/users')

// Root Endpoint
router.get('/', index.index);

// Post API Collection
router.get('/api/v1/posts', authenticate, post.index);
router.post('/api/v1/posts', authenticate, post.create);

router.use('/api/v1/users', userRoutes);

// Test API Collection
router.post('/test', uploader.single('image'), test.index);
router.post('/imagekit', uploader.single('image'), test.uploadToCDN);
module.exports = router;