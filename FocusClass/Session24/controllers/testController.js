const fs = require('fs');
const path = require('path');

const imagekit = require('../lib/imagekit')

module.exports = {

  // Cara manual
  index(req, res) {

    // let filename = req.file.originalname.split('.');
    // filename = `IMG-${Date.now()}.${filename[filename.length - 1]}`;

    // fs.writeFileSync(
    //   path.resolve(__dirname, '..', 'uploads', filename),
    //   req.file.buffer
    // );
    res.send(req.file.url);
  },

  async uploadToCDN(req, res) {
  try {
    console.log(req.file.buffer)
      const file = await imagekit.upload({file: req.file.buffer, fileName: req.file.originalname});
      console.log(file);
      
      res.json(file);
    } catch (error) {
      res.json(error);
      
    }
  }

  // Instant
}