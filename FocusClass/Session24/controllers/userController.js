const { User } = require('../models');

exports.register = async function (req, res) {
  try {
    const user = await User.create({
      name: req.body.name,
      email: req.body.email,
      encrypted_password: req.body.encrypted_password
    })
  } catch (err) {
    res.status(422);
  }
}
