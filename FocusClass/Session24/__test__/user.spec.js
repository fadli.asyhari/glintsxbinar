const request = require('supertest');
const app = require('../index');
const { User } = require('../models');

describe.skip('User API Collection', () => {

    beforeAll(() => {
        return User.destroy({
            truncate: true
        })
    })

    afterAll(() => {
        return User.destroy({
            truncate: true
        })
    })

    describe('POST /api/v1/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'Geiki Sen',
                    email: 'geikisen@gmail.com',
                    encrypted_password: 'ojolaliyo'
                })
                .then(res => {
                    except(res.statusCode).toEqual(201);
                    except(res.body.status).toEqual('success');
                    except(res.body.data).toHaveProperty('user');
                    done();
                })
        })
    })
})