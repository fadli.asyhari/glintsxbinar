const request = require('supertest');
const app = require('../index');
const { Post } = require('../models');

const jwt = require('jsonwebtoken')

// describe('Post  API Collection', () => {

//     beforeAll(() => {
//         return Post.destroy({
//             truncate: true
//         })
//     })

//     afterAll(() => {
//         return Post.destroy({
//             truncate: true
//         })
//     })

//     describe('POST /api/v1/posts', () => {
//         test('Should successfully create a new post', done => {
//             request(app).post('/api/v1/posts')
//               .set('Content-Type', 'application/json')
//               .send({ title: 'Hello World', body: 'Lorem Ipsum'})
//               .then(res => {
//                   expect(res.statusCode).toEqual(201);
//                   expect(res.body.status).toEqual('success');
//                   expect(res.body.data).toHaveProperty('post');
//                   done();
//               })
//         })
//     })
// })

describe('Posts API Collection', () => {

    describe('GET /api/v1/posts', () => {
        test('Should allow authenticated user', async () => {
            const token = jwt.sign({ name: 'fadli', email: 'fadli@gmail.com' }, process.env.SECRET_KEY)

            const response = await request(app)
                .get('/api/v1/posts')
                .set('authorization', token)

            expect(response.status).toEqual(200)
            expect(response.body.status).toEqual('success');
        })

        test('Should NOT allow authenticated user', async () => {
            const response = await request(app).get('/api/v1/posts')

            expect(response.status).toEqual(401)
            expect(response.body).toEqual({ ok: false });
        })

        test('Should successfully get all posts', async () => {
            const token = jwt.sign({ name: 'fadli', email: 'fadli@gmail.com' }, process.env.SECRET_KEY)
            const response = await request(app).get('/api/v1/posts').set('authorization', token)

            expect(response.status).toEqual(200);
            expect(response.body.status).toEqual('success');
            expect(response.body.data).toHaveProperty('posts');
        })


    })

    describe('POST /api/v1/posts', () => {

    })

})