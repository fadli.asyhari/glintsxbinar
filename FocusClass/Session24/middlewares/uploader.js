const multer = require('multer');
const config = require('../config/uploader.js');
const env = process.env.NODE_ENV || 'development';

module.exports = multer(config[env]);