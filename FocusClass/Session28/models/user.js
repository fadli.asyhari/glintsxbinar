'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    underscored: true,
  });
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};


User._encrypted = password => bcrypt.hashSync(password, 10)

User.register = async function ({ email, password }) {
  try {
    const intance = await this.create({ email, password: this })
    const verificationToken = await jwt.sign(instance.entity, SECRET_KEY)
    await mailer.send({
      to: email,
      from: 'no-reply@example.com',
      subject: 'Email Verification',
      html: `<strong>Hello, please verify your email by clicking this link ${BASE_URL}/api/v1/auth/verify?token=${token}</strong>`
    })
    return Promise.resolve()
  }
}

