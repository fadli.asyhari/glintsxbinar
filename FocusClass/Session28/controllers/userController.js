const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const sendgrid = require('../lib/sendgrid');
const mailer = require('@sendgrid/mail');
mailer.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
    async register(req, res) {
        const userEmail = await User.findOne({
            where: {
                email: req.body.email
            }
        })
        try {
            if (!userEmail) {
                const hashedPassword = await bcrypt.hash(req.body.password, 10);
                let newUser = await User.create({
                    email: req.body.email.toLowerCase(),
                    password: hashedPassword
                })
                const token = await jwt.sign({
                    id: newUser.id,
                    email: newUser.email
                }, process.env.SECRET_KEY)
                res.status(201).json({
                    "status": "success",
                    data: { newUser, token }
                })
                await mailer
                    .send({
                        to: email,
                        from: 'no-reply@example.com',
                        subject: 'Email Verification',
                        html: `<strong>Hello, please verify your email by clicking this link ${BASE_URL}/api/v1/auth/verify?token=${token}</strong>`
                    })
                    .then(() => { }, error => {
                        console.error(error);

                        if (error.response) {
                            console.error(error.response.body)
                        }
                    })
            } else {
                throw new Error('Email already registered');
            }
        } catch (err) {
            console.log(err)
            res.status(400).json({
                "status": "failed",
                "message": [err.message]
            })
        }
    },
    async login(req, res) {
        let user = await User.findOne({
            where: {
                email: req.body.email
            }
        })
        if (user == null) {
            return res.status(400).json({
                "status": "failed",
                "message": "user doesn't exist or email must be fill with lowercase"
            })
        }
        const token = await jwt.sign({
            id: user.id,
            email: user.email
        }, process.env.SECRET_KEY)
        try {
            if (await bcrypt.compare(req.body.password, user.encrypted_password)) {
                res.status(201).json({
                    "status": "success",
                    data: { token }
                })
            } else {
                res.status(400).json({
                    "status": "failed",
                    "message": "wrong password"
                })
            }
        }
        catch (err) {
            res.status(500).json({
                "status": "failed",
                "message": [err.message]
            })
        }
    },
    async userData(req, res) {
        try {
            let userData = await User.findByPk(req.user.id, {
                include: [Profile]
            });
            res.status(200).json({
                "status": "Success",
                data: { userData }
            })
        } catch (err) {
            res.status(500).json({
                status: 'fail',
                message: err
            })
        }
    },

    async verify(req,res) {
        try {

        } catch (err) {
            
        }
    }
}