const router = require('express').Router();
const user = require('./controllers/userController');

router.post('/user/register', user.register);
router.post('/user/login', user.login);
router.get('/user/verify', user.verify)

module.exports = router;