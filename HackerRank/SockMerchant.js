'use strict';

const fs = require('fs');
const ws = fs.createWriteStream(process.env.OUTPUT_PATH);
process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the sockMerchant function below.
function sockMerchant(n, ar) {
    let total = 0;
    let outArr = []
    for (let apapun of ar) {
        outArr[apapun] != null ? outArr[apapun] += 1 : outArr[apapun] = 1

        //ws.write(outArr[apapun]+ " ")
        if (outArr[apapun] == 2) {
            outArr[apapun] = 0;
            total += 1;
        }
        //ws.write(apapun+ " ")

    }
    return total
}



function main() {


    const n = parseInt(readLine(), 10);

    const ar = readLine().split(' ').map(arTemp => parseInt(arTemp, 10));

    let result = sockMerchant(n, ar);

    ws.write(result + "\n");

    ws.end();
}
