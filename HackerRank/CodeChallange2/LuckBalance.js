'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the luckBalance function below.
function luckBalance(k, contests) {
 let importantContests = []
  let unimportantContests = []
  let winCount = 0

  for (let i = 0; i < contests.length; i++) {
    if (contests[i][1] === 1)
      ++winCount && importantContests.push(contests[i])
    else
      unimportantContests.push(contests[i])
  }
  importantContests.sort((a, b) => a[0] - b[0])
  winCount -= k 
  const winContests = importantContests.splice(0, winCount) 

  let luck = 0
  for(let i = 0; i < contests.length; i++) {
    if (unimportantContests[i]) luck += unimportantContests[i][0]
    if (importantContests[i]) luck += importantContests[i][0]
    if (winContests[i]) luck -= winContests[i][0] 
  }

  return luck

}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const nk = readLine().split(' ');

    const n = parseInt(nk[0], 10);

    const k = parseInt(nk[1], 10);

    let contests = Array(n);

    for (let i = 0; i < n; i++) {
        contests[i] = readLine().split(' ').map(contestsTemp => parseInt(contestsTemp, 10));
    }

    const result = luckBalance(k, contests);

    ws.write(result + '\n');

    ws.end();
}
