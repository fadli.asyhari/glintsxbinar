'use strict';

const fs = require('fs');
 const ws = fs.createWriteStream(process.env.OUTPUT_PATH);
process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the substrCount function below.
function substrCount(n, s) {
    let total=n;
    let ganjil=-1;
    for(let i=0;i<n;i++)
    {
        let j=1;
        //GANJIL
        while(true)
        {
            //ws.write(s[i] + '\n');
            
            if((i-j < 0 ) || i+j-1 >= n) break
            else if(s[i-j] == s[i+j-1])
            {
                if(s[i-j] != s[i-1]) break
                total++
                j++;
                //ws.write('0 ');
            }
            else break;
        }
        j=1;
        //GENAP
        while(true)
        {
            //ws.write(s[i] + '\n');
            
            if((i-j < 0 ) || i+j >= n) break
            else if(s[i-j] == s[i+j])
            {

                if(s[i-j] != s[i-1]) break

                total++
                j++;
                //ws.write('1 ');
            }
            else break
        }
    }
    return total

}

function main() {
   

    const n = parseInt(readLine(), 10);

    const s = readLine();

    const result = substrCount(n, s);

    ws.write(result + '\n');

    ws.end();
}
