const { Product } = require('../models');

module.exports = {
  all(req, res) {
    Product.findAll().then(data => {
      res.status(200).json({
        status: true,
        data
      })
    })
  },

  available(req, res) {
    res.status(200).json({})
  }
}
