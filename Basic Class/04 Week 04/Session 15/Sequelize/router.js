const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');
const user = require('./controllers/userController');

/* Product API Collection */
router.get('/products', product.all);
router.get('/products/available', product.available)

/* User API Collection */

module.exports = router;
