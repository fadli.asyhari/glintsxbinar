const cube = require('../db/cubeVolume.json');
module.exports = {
    cubeVolume(req, res){
        let result = Math.pow(req.body.S,3);
        return res.status(200).json({
            "Name": 'Cube',
            "Side": req.body.S,
            "Volume": result
        })
    }
}
