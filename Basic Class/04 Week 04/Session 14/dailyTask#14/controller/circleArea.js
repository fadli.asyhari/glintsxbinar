const circle = require('../db/circleArea.json');
module.exports = {
    circleArea(req, res){
        let result = 2 * req.body.Phi * req.body.R;
        return res.status(200).json({
            "Name": 'Circle',
            "Phi": req.body.Phi,
            "R": req.body.R,
            "Area": result
        })
    }
}
