// Importing Module
const readline = require('readline');
const array = require('./array.js');
const hello = require('./function.js');
const object = require('./object.js');

object.lagi();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question("What is your name? ", name => {
  console.log(`Hi, ${name}`);
  rl.close();
})
