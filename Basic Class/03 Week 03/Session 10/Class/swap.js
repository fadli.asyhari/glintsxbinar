// Swap a value with b value;
let a = 10;
let b = 20;
console.log('a Before:', a);
console.log('b Before:', b);

// Swapper
let c = a;

a = b;
b = c;

console.log('a After:', a);
console.log('b After', b);

// Swap array?
let arr = ["Hello World", "Goodbye World", "Kata-kata Lain"];
let tmp = arr[1]; // Backup
arr[1] = arr[0];
arr[0] = tmp;
console.log('Swapped:', arr);
