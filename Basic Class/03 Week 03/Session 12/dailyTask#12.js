const fs = require('fs');

class Record {
  constructor(props) {
    if (this.constructor == Record) 
      throw new Error("Can't instantiate from Record");
    
    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Props must be an object");
    

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`)
    })
  }

  _set(props) {
    this.constructor.properties.forEach(i => {
      this[i] = props[i];
    })
  }

  get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`)
          .toString()
      )
    }
    catch {
      return []
    }
  }


  find(id) {

  }

  update(id) {

  }

  delete(id) {

  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify([...this.all, { id: this.all.length + 1, ...this } ], null, 2)
    );
  }

}

class User extends Record {

  static properties = [
    "email",
    "password"
  ]
}

let Fikri = new User({
  email: "test01@mail.com",
  password: "123456"
});

let Fikri2 = new User({
  email: "test02@mail.com",
  password: "123456"
});

Fikri.save();
Fikri2.save();


class Book extends Record {
    static properties = [
        "title", 
        "author", 
        "price", 
        "publisher"]

   
   save(){
    this.all.forEach(i => {
      if (i.title == this.title) {
        throw new Error('It already exists');
      }
    })
    super.save();
     
}
}



class Product extends Record {

    static properties = [
        "name",
        "price",
        "stock"
    ]

    save(){
        this.all.forEach(i => {
          if (i.name == this.name) {
            throw new Error('It already exists');
          }
        })
        super.save();
}
}

let Shirah = new Book({
    title: 'Shirah Nabawiyah',
    author: 'Syekh Syaikhurrahman Al Mubarakfuri',
    price: 80000,
    publisher: 'Al Ikhlas'
})

let Underwear = new Product({
    name: 'Under Armour Underwear',
    stock: 143,
    price: 69142,
  })

let esBatu = new Product({
    name: 'Batu Beku',
    stock: 2,
    price: 500,
  })

  
/*
  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/

Shirah.save();
Underwear.save();
esBatu.save();
