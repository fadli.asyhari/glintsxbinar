// To randomly call element inside the array
Object.defineProperty(Array.prototype, 'sample', {
    get: function() {
      return this[
        Math.floor(
          Math.random() * this.length
        )
      ]
    }
  })
  
  Array.prototype.random = function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
  
class Human {
    static properties = [
        "name",
        "citizenship",
        "food"
    ]

    constructor(props) {
        this._validate(props);

        this.name = props.name;
        this.citizenship = props.citizenship;
        this.food = props.food;
    }
    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
          throw new Error("Constructor needs object to work with");
    
        this.constructor.properties.forEach(i => {
          if (!Object.keys(props).includes(i))
            throw new Error(`${i} is required`);
        });
      }
      introduce() {
        console.log(`Hi, my name is ${this.name}, and I'm ${this.citizenship}.`);
      }

      cook() {
        console.log(`${this.name} is cooking ${this.food.sample}`);
      }

}

class Chef extends Human {
    static properties = [...super.properties, "cuisines", "chefType",]

    constructor(props){
        super(props);
        this.cuisines = props.cuisines;
        this.chefType = props.chefType;
    }
    introduce() {
        super.introduce();
        console.log(`And I'm a ${this.chefType} Chef.`);
      }

    cook(){
        console.log(`${this.name} is cooking ${this.cuisines.sample}.`);
    }
    promote(){
        console.log(`i can cook ${this.cuisines}`)
    }
       

}

const Fadli = new Chef ({
    name : 'Fadli',
    citizenship : 'Italian',
    food : ['Water', 'Egg'],
    cuisines : ['Pizza','Burger','Soup'],
    chefType : 'French'
})

Fadli.introduce();
Fadli.cook();
Fadli.promote();
