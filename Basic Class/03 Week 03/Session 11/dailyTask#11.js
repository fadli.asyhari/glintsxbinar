class Human {
    /* so there's some human, they have a name, of course, they know some language,
       they can greet another human, they can marry another human, they can introduce
       themself, and they can learn new language from their spouse.
    */
   constructor (name, language){ //instance property
       this.name = name ; 
       this.language = language ;
   }
   static isMarried = false ; //static property

   greet(target) { // greeting section
    console.log(`\nHi ${target.name}! I'm ${this.name}!`);
}

#doMarry = function(target){ // marry section
    this.isMarried = true;
    target.isMarried = true;
    this.partner = target.name;
    target.partner = this.name;
    console.log(`${this.name} married with ${target.name}`)
}

introduce(){  // intro section
    console.log(`\nHi! My name is ${this.name}!`)

}

learnNewLang(l){ // learn language section
    console.log("\nbefore Learning : ", this.language);
    this.language.push(l);
    console.log("after Learning : ", this.language);

}

marry(target){ //marry and learn new language section
    if(!target.isMarried && !this.isMarried){
        let marryNow = false;
        this.language.forEach((l) => {
            if(target.language.indexOf(l) != -1) {
                marryNow = true;
            }
        })
        if(!marryNow){
            console.log("\nLearn the same language first!");
            this.learnNewLang(target.language[Math.floor(Math.random() * (target.language.length))]);
            console.log("My Language: ",this.language, "\nPartner Language :", target.language);
            this.isMarried = true;
        }
        console.log("\nLet's marry now");
        this.#doMarry(target);            
    } else {
        console.log("\nCan't marry yet!")
    }
}

}

let Alpha = new Human("Alpha" , ["Bahasa"]);
let Beta = new Human("Beta" , ["Bahasa"],["Francais"]);
let Omega = new Human("Omega" , ["English"]);

Alpha.greet(Beta); // Hi Beta! I'm Alpha!

Alpha.introduce(); // Hi! My name is Alpha!

Alpha.learnNewLang("Francais"); /* before Learning :  [ 'Bahasa' ]
                                   after Learning :  [ 'Bahasa', 'Francais' ] */

Alpha.marry(Beta); /* Let's marry now
                      Alpha married with Beta */

Beta.marry(Omega); // Can't marry yet!
