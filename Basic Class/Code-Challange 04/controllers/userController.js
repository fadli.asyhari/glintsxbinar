const { User } = require('../models');

exports.register = async function(req, res, next) {
  try {
    const user = await User.create({
      email: req.body.email,
      encrypted_password: req.body.password
    })

    res.data = user;
    next();
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.login = async function(req, res, next) {
  try {
    const user = await User.authenticate(req.body);
    res.status(201);
    res.data = { user: user.entity }
    next()
  }

  catch(err) {
    res.status(401);
    next(err);
  }
}

exports.verify = async function(req, res, next) {
  try {
    if (!req.user.verified) {
      await req.user.update({ verified: true });
      await Profile.create({ user_id: req.user.id });
    }
    res.status(200);
    res.data = "Verified!";
    next()
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.me = async function(req, res, next) {
  res.status(200);
  res.data = req.user.entity;
  next()
}

