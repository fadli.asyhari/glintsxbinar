const { Post, User } = require('../models');

exports.create = async function(req, res, next) {
  try {
    const post = await Post.create({
      title: req.body.title,
      body: req.body.body,
      user_id: req.user.id
    });
    res.status(200);
    res.data = { post };
    next();
  }
  
  catch(err) {
    res.status(422);
    next(err);
  };
}

exports.findAll = async function(req, res, next) {
  try {
    const posts = await Post.findAll({
      include: 'author'
    });
    if(!res.admin) {
      res.data = res.data.filter((a) => a.approved == true);
    }
    next();
  }

  catch(err) {
    res.status(404);
    next(err);
  }
}

exports.update = async function(req, res, next) {
   try {
    if (req.body.approved !== null) {
      delete req.body.approved
    }
    await Post.update(req.body, {
      where: { id: req.params.id }
    });
    res.status(200);
    res.data = await Post.findByPk(req.params.id);
    next();
  }
  
  catch(err) {
    res.status(422);
    next(err);
  };
}

exports.delete = async function(req, res, next) {
  try {
    await Post.destroy({ where: { id: req.params.id } });
    res.status(203);
    res.end();
  } catch (error) {
    res.status(400);
    next(error);
  }
}

exports.approved = async function(req, res, next) {
  if (res.admin) {
    await Post.update({ approved: true }, { where: { id: req.params.id } });
    res.data = await Post.findByPk(req.params.id);
    next();
  } else {
    res.status(403);
    next(new Error("You're not the admin!"))
  }
}
