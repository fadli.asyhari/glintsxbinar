const express = require('express');
const morgan = require('morgan');
const app = express();

const exception = require('./middlewares/exception');
const router = require('./router');

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
})

app.use('/api/v1', router);

exception.forEach(handler =>
  app.use(handler)
)

app.listen(3000, () => {
  console.log(`Listening on port 3000`);
})
