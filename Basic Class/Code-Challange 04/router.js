const router = require('express').Router();

// Core Module
const userController = require('./controllers/userController');
const postController = require('./controllers/postController');

// Middleware
const success = require('./middlewares/success');
const authenticate = require('./middlewares/authenticate');
const checkOwnership = require('./middlewares/checkOwnership');
const checkAdministrator = require('./middlewares/checkAdministrator');

// User Collection API
router.post('/users/register', userController.register, success);
router.post('/users/login', userController.login, success);
router.get('/users/verify', authenticate, userController.verify, success);
router.get('/users/me', authenticate, userController.me, success);

// Product Collection API
router.post('/posts', authenticate, postController.create, success);
router.get('/posts', checkAdministrator, postController.findAll, success);
router.put('/approve/posts/:id', authenticate, checkAdministrator, postController.approved, success);
router.put('/posts/:id', authenticate, checkOwnership('Post'), postController.update, success);

module.exports = router;
