const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    if (!token) {
      res.status(401);
      next(new Error("Can't go without token!"));
    }  
    let payload = jwt.verify(token, process.env.SECRET_KEY);
    req.user = await User.findByPk(payload.id);
    next(); 
  }

  catch {
    res.status(401);
    next(new Error("Invalid Token"));
  }
}
