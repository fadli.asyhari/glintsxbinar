const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, process.env.SECRET_KEY);
    let foundUser = await User.findByPk(payload.id);
    if (foundUser.roles == "admin") {
      res.admin = true;
    }
    next();
  } catch (error) {
    next();
  }
};
