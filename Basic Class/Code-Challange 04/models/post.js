'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    user_id: DataTypes.INTEGER
  }, {
    underscored: true,
  });
  Post.associate = function(models) {
    Post.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'author'
    });
  };
  return Post;
};