const router = require('express').Router();

// Core Module
const userController = require('./controllers/userController');
const postController = require('./controllers/postController');

// Middleware
const success = require('./middlewares/success');
const authenticate = require('./middlewares/authenticate');
const editor = require('./middlewares/editor'); 

// User Collection API
router.post('/users/register', userController.register, success);
router.post('/users/login', userController.login, success);

// Product Collection API
router.post('/posts', authenticate, postController.create, success);
router.get('/posts', postController.findAll, success);
router.put('/posts/:id', authenticate, editor, postController.update, success);
router.delete("/posts/:id", authenticate, editor, postController.delete, success);
  
  

module.exports = router;
