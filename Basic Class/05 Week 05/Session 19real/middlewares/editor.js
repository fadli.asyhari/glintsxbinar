const { Post } = require("../models");

module.exports = async (req, res, next) => {
  try {
    let target = await Post.findByPk(req.params.id);
    if (target.user_id == req.user.id) {
      next();
    } else {
      res.status(403);
      next(new Error("Forbidden, this ain't yours!"));
    }
  } catch (error) {
    res.status(401);
    next(error);
  }
};
